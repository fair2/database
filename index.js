
const fs = require('fs');
const path = require("path");

class DataBase {
    constructor(databaseFile, defaultData, pretty) {
        this.data = {};
        this.pretty = pretty ? true : false;
        this.open(databaseFile, defaultData);
    }

    backup(fileName = 'backup', folder = '') {
        if (fs.existsSync(this.file)) {
            if (folder.length > 0) {
                var baseFileName = path.basename(this.file).replace('.json', '');
                fs.copyFileSync(this.file, path.resolve(folder + baseFileName + "_" + fileName + '.json'));//{folder}{baseFileName}_{backupFileName}.json
            } else {
                fs.copyFileSync(this.file, path.resolve(this.file.replace('.json', '') + "_" + fileName + '.json'));//{file}_{backupFileName}.json
            }
        }
    }

    open(databaseFile, defaultData) {
        if (databaseFile) this.file = this.getFilePath(databaseFile);
        try {
            if (fs.existsSync(this.file)) {
                this.data = defaultData !== undefined ? defaultData : {};
                Object.assign(this.data, JSON.parse(fs.readFileSync(this.file)));
                this.save();
            }
            else if (defaultData) {
                this.save(defaultData, true);
                this.data = defaultData;
            }
            else {
                this.data = undefined;
                return undefined;
            }
        } catch (err) {
            console.error(err)
            this.data = undefined;
            return undefined;
        }
        return this.data;
    }

    close() {
        this.data = null;
        this.file = null;
    }

    deprecate() {
        if (fs.existsSync(this.file)) {
            fs.copyFileSync(this.file, this.file.replace('.json', '_deprecated.json'));
            fs.unlinkSync(this.file);
            this.close();
        }
    }

    save(data, instant = false) {
        if (!data) {
            data = this.data;
        } else {
            this.data = data;
        }
        if (data === undefined) {
            throw new Error('whoops, seems like you are trying to save undefined to a database file')
        }
        clearTimeout(this.writeToFileTimeout);
        if (instant) {
            this.writeToFile();
        } else {
            this.writeToFileTimeout = setTimeout(() => {
                this.writeToFile();
            }, 500);
        }
    }

    writeToFile() {
        if (!this.data || !this.file) {
            console.error("Unable to save, database is closed");
            return;
        }
        if (this.pretty) {
            fs.writeFileSync(this.file, JSON.stringify(this.data, null, 4), (err) => { if (err) console.log(err); });
        } else {
            fs.writeFileSync(this.file, JSON.stringify(this.data), (err) => { if (err) console.log(err); });
        }
    }

    isEmpty() {
        for (var key in this.data) {
            if (this.data.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    copy(target) {
        if (target === undefined) {
            target = this.data;
        }
        return JSON.parse(JSON.stringify(target));
    }

    getFilePath(file) {
        if (file) this.file = file;
        return path.resolve(this.file);
    }

    getNow() {
        return Date.now();
    }
}

module.exports = DataBase;