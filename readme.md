# Database 
A simple database engine that runs on JSON, it will save to a file.

## install
Use the npm engine to install this package.
```
npm install bitbucket:fair2/database
```

## code example
```
var Database = require("database");

var db = new Database('db.json', { default: "data" }, true);

console.log(db.data);

db.data.default = "dataChanged";

db.data.users = [];
db.data.users.push({ account: "user", test: true });

db.save();
```

## open database
```
var db = new Database('db.json', { default: "data" }, true);
```
```
var db = new Database(fileName, defaultData, prettyJSON);
```
If you open a database with default data, it will use this data, if the file doesn't yet exist. 
   
As soon as the file is created (and the script runs again) the data will not be overwritten with the default data.
  
If you want to read JSON, use prettyJSON = true, else ommit. Ommiting save disk space.
